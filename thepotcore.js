/*
 * ThePotCore
 *
 * Version: 1.0
 *  Author: Sara Potyscki (ThePot)
 * Website: http://www.thepot.site/
 * License: Released under the MIT license [http://www.thepot.site]
 *
 */


window.getNode = function(arg){
	return document.querySelector(arg) || document.createElement("div");
}

Element.prototype.addClass = function (arg) {
	if (!this.hasClass(arg)) {
		this.className += " " + arg;
		this.className = this.className.trim();
	}
	return this;
};
Element.prototype.removeClass = function (arg) {
	this.className = ((" "+this.className+" ").replace(" "+arg+" "," ")).trim();
	return this;
};
Element.prototype.hasClass = function (arg) {
	return (" "+this.className+" ").indexOf(" "+arg+" ") > -1;
};
Element.prototype.getChildren = function (arg) {
	var by =  arg.charAt(0);
	var val = arg.substring(1);

	var found = [];

	var children = this.children;
    for (var i = 0; i < children.length; i++) {
		if (by == ".") {
			if (children[i].className && children[i].className.split(' ').indexOf(val) >= 0){
				found.push(children[i]);
			}

			var match = children[i].getChildren(by+val);
			found = found.concat(match);
		}
    }
    return found;
};

window.mergeJSON = function() {
    var destination = {},
        sources = [].slice.call( arguments, 0 );
    sources.forEach(function( source ) {
        var prop;
        for ( prop in source ) {
            if ( prop in destination && Array.isArray( destination[ prop ] ) ) {

                // Concat Arrays
                destination[ prop ] = destination[ prop ].concat( source[ prop ] );

            } else if ( prop in destination && typeof destination[ prop ] === "object" ) {

                // Merge Objects
                destination[ prop ] = mergeJSON( destination[ prop ], source[ prop ] );

            } else {

                // Set new values
                destination[ prop ] = source[ prop ];

            }
        }
    });
    return destination;
};
